#include <array>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <vector>

const std::string game{
    "W1.e4 B1.c6 W2.d4 B2.d5 W3.exd5 B3.cxd5 W4.Bd3 B4.Nc6 W5.c3 B5.Nf6 W6.Bf4 "
    "B6.Bg4 W7.Qb3 B7.Qd7 W8.Nd2 B8.e6 W9.Ngf3 B9.Bd6 W10.Ne5 B10.Bxe5 "
    "W11.dxe5 B11.Nh5 W12.Be3 B12.a6 W13.h3 B13.Nxe5 W14.Bf1 B14.Bf5 W15.g4 "
    "B15.Nd3+ W16.Bxd3 B16.Bxd3 W17.gxh5 B17.Rc8 W18.Rg1 B18.f6 W19.h6 B19.g6 "
    "W20.O-O-O B20.d4 W21.Bxd4 B21.Ke7 W22.Nf3 B22.e5 W23.Rxd3 B23.exd4 "
    "W24.Re1+ B24.Kf8 W25.Rxd4 B25.Qc6 W26.Qb4+"};

int main() {

  // Define pieces
  const std::map<char, std::string> pieces{
      {'K', "King"},   {'Q', "Queen"},  {'R', "Rook"},
      {'B', "Bishop"}, {'N', "Knight"}, {'P', "Pawn"},
  };

  // Print pieces
  std::for_each(pieces.cbegin(), pieces.cend(), [](const auto &piece) {
    std::cout << piece.first << "\t" << piece.second << "\n";
  });

  // Create board
  std::array<std::string, 8 * 8> board;

  // Print board
  std::for_each(board.begin(), board.end(),
                [x = 0ul, y = 0ul](auto &square) mutable {
                  const size_t dimension = 8;
                  square = std::to_string(x) + std::to_string(y) + " ";

                  std::cout << square;
                  if (y == dimension - 1)
                    std::cout << "\n";

                  ++x %= dimension;
                  ++y %= dimension;

                  assert(x < dimension);
                  assert(y < dimension);
                });

  // Play game

  // Find each move
  std::stringstream ss(game);
  const std::vector<std::string> moves{std::istream_iterator<std::string>{ss},
                                       {}};

  // Print moves
  std::for_each(moves.cbegin(), moves.cend(), [n = 1](const auto &_) mutable {
    std::cout << _.substr(_.find_last_of('.') + 1) << "\t";
    if (!(n++ % 2))
      std::cout << "\n";
  });

  std::cout << "\n";
}
