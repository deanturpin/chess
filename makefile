# Get test file, build and run
all: main.o run

# Compiler of choice
CXX := g++-10

# Dependencies
apt:
	apt update
	apt install --yes g++-10 \
		time cppcheck valgrind

# Some useful compiler flags not set by default
CXXFLAGS ?= --std=c++20 --all-warnings --extra-warnings --pedantic-errors \
	-Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy -Wconversion \
	-Wall -Wextra -Wpedantic \
	-Og \
	-Wattribute-alias -Wformat-overflow -Wformat-truncation -Wclass-conversion \
	-Wmissing-attributes -Wstringop-truncation $(EXTRA_FLAGS)

# Make an object from a cpp
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $<

# Run and time
run: main.o
	/usr/bin/time ./$<

# Tidy up
clean:
	rm -f *.o

##########
# TOOLS
##########

format:
	clang-format-10 -i *.cpp

valgrind: main.o
	valgrind --tool=cachegrind ./$<

lint:
	cppcheck --enable=all main.cpp
 
